﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace c
{
    public class BucleNumeros
    {
        public static void Run()
        {
            List<int> lstnumeros = new List<int>();
            int primPosi = 11;

            for (int i = 1; i <= 5; i++)
            {
                for (int j = 1; j <= 5; j++)
                {
                    lstnumeros.Add(primPosi);
                    primPosi++;
                }

                primPosi += 5;
            }
            lstnumeros.ToList().ForEach(w => Console.WriteLine(w));

        }
    }
}
