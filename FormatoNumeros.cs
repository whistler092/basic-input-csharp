﻿    using System;
namespace c
{
    public class FormatoNumeros
    {
        public static void Run()
        {

            Console.WriteLine("Ingrese la cantidad de veces que quiere repetir el programa");
            int bucle = int.Parse(Console.ReadLine());


            for (int i = 1; i <= bucle; i++)
            {

                Console.WriteLine("Ingrese un numero del 0 al 9.999.999");
                decimal numero = decimal.Parse(Console.ReadLine().Replace(".",""));

                if (!(numero >= 0 && numero <= 9999999))
                {
                    Console.WriteLine("El numero a ingresar debe de estar dentro del rango requerido");
                    return;

                }
                String numeroString = numero.ToString();
                int formatNum = numeroString.Length;

                if (formatNum == 7)
                {
                    String millones = numeroString.Substring(0, 1);
                    Console.WriteLine("Millones: " + millones);

                    String cientosMiles = numeroString.Substring(1, 1);
                    Console.WriteLine("Cientos Miles: " + cientosMiles);

                    String decenasMiles = numeroString.Substring(2, 1);
                    Console.WriteLine("Decenas Miles: " + decenasMiles);

                    String unidadesMiles = numeroString.Substring(3, 1);
                    Console.WriteLine("Unidades Miles: " + unidadesMiles);

                    String cientos = numeroString.Substring(4, 1);
                    Console.WriteLine("Cientos: " + cientos);

                    String decenas = numeroString.Substring(5, 1);
                    Console.WriteLine("Decenas: " + decenas);

                    String unidades = numeroString.Substring(6, 1);
                    Console.WriteLine("Unidades: " + unidades);


                }
                else if (formatNum == 6)
                {
                    String cientosMiles = numeroString.Substring(0, 1);
                    Console.WriteLine("Cientos Miles: " + cientosMiles);

                    String decenasMiles = numeroString.Substring(1, 1);
                    Console.WriteLine("Decenas Miles: " + decenasMiles);

                    String unidadesMiles = numeroString.Substring(2, 1);
                    Console.WriteLine("Unidades Miles: " + unidadesMiles);

                    String cientos = numeroString.Substring(3, 1);
                    Console.WriteLine("Cientos: " + cientos);

                    String decenas = numeroString.Substring(4, 1);
                    Console.WriteLine("Decenas: " + decenas);

                    String unidades = numeroString.Substring(5, 1);
                    Console.WriteLine("Unidades: " + unidades);
                }
                else if (formatNum == 5)
                {
                    String decenasMiles = numeroString.Substring(0, 1);
                    Console.WriteLine("Decenas Miles: " + decenasMiles);

                    String unidadesMiles = numeroString.Substring(1, 1);
                    Console.WriteLine("Unidades Miles: " + unidadesMiles);

                    String cientos = numeroString.Substring(2, 1);
                    Console.WriteLine("Cientos: " + cientos);

                    String decenas = numeroString.Substring(3, 1);
                    Console.WriteLine("Decenas: " + decenas);

                    String unidades = numeroString.Substring(4, 1);
                    Console.WriteLine("Unidades: " + unidades);
                }
                else if (formatNum == 4)
                {
                    String unidadesMiles = numeroString.Substring(0, 1);
                    Console.WriteLine("Unidades Miles: " + unidadesMiles);

                    String cientos = numeroString.Substring(1, 1);
                    Console.WriteLine("Cientos: " + cientos);

                    String decenas = numeroString.Substring(2, 1);
                    Console.WriteLine("Decenas: " + decenas);

                    String unidades = numeroString.Substring(3, 1);
                    Console.WriteLine("Unidades: " + unidades);
                }
                else if (formatNum == 3)
                {
                    String cientos = numeroString.Substring(0, 1);
                    Console.WriteLine("Cientos: " + cientos);

                    String decenas = numeroString.Substring(1, 1);
                    Console.WriteLine("Decenas: " + decenas);

                    String unidades = numeroString.Substring(2, 1);
                    Console.WriteLine("Unidades: " + unidades);
                }
                else if (formatNum == 2)
                {
                    String decenas = numeroString.Substring(0, 1);
                    Console.WriteLine("Decenas: " + decenas);

                    String unidades = numeroString.Substring(1, 1);
                    Console.WriteLine("Unidades: " + unidades);
                }
                else if (formatNum == 1)
                {
                    String unidades = numeroString.Substring(0, 1);
                    Console.WriteLine("Unidades: " + unidades);
                }

            }

            Console.WriteLine("El programa ha finalizado");
        }
    }
}
