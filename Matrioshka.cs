﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace c
{
    public class Matrioshka
    {
        public static void Run()
        {
            int recorrido;
            List<string> lstMunecas = new List<string>();
            
            recorrido = 10;
            
            if (recorrido <= 20) 
            {
                for (int i = 1; i <= recorrido ; i++) 
                {
                    string tipoMu = $"Muñeca con tamaño: {i} cm de alto";
                    lstMunecas.Add(tipoMu);
                    
                }

                lstMunecas.ToList().ForEach(w => Console.WriteLine(w));
            }
            else
            {
                Console.WriteLine("El numero de munecas no puede ser mayor"
                        + "a 20 ya que supera el limite de tamanos");
            }
        
        }
    }
}
