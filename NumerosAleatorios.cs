﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace c
{
    public static class NumerosAleatorios
    {
        public static void Run()
        {
            //Scanner scanner = new Scanner(System.in);

            List<int> lstnumeros = new List<int>();

            Console.WriteLine("Ingrese la cantidad de veces que quiere repetir el programa");
            var bucle =  int.Parse(Console.ReadLine());

            if (bucle < 0)
            {
                Console.WriteLine("Ingrese un numero mayor a 1.");
                return;
            }

            for (int j = 1; j <= bucle; j++)
            {
                for (int i = 1; i <= 9; i++)
                {
                    Thread.Sleep(100);
                    var segundos = (int)((DateTime.Now.Ticks / 1000) % 3600);

                    lstnumeros.Add(segundos);
                }

                lstnumeros.ToList().ForEach(w => Console.Write(w));
                lstnumeros.Clear();
            }

           
        }
    }
}
