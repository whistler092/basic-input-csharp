﻿using System;
namespace c
{
    public static class Primos
    {
        public static void Run()
        {
            int div = 0, raiz = 0;

            for (int i = 1; i <= 100; i++)
            {
                div = 0;
                raiz = (int)Math.Sqrt(i);
                for (int j = 1; j <= raiz; j++)
                {
                    if (i % j == 0)
                        div++;
                }
                if (div <= 1)
                    Console.WriteLine(i + " es numero primo");
            }
            
        }
    }
}
